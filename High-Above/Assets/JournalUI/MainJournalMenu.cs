﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainJournalMenu : MonoBehaviour
{

    public void PlayGame()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1.0f;
    }

    public void MainMenu()
    {
        
        SceneManager.LoadScene(0);
    }
    
}
