﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour
{
    public GameObject envelope;
    JournalEvents journal;
    PlayerMovementScript moveScript;
    public GameObject DialogueScreen;
    public static NarrativeDialogue narrative;

    public GameObject yesText;
    public GameObject noText;

    public bool showYes = false;
    //public bool showNo = false;
    public bool showEnvelope = false;

    public float timer = 0f;
    public bool isTimerTet = false;

    // Start is called before the first frame update
    void Start()
    {
        journal = GameObject.Find("EventHandle").GetComponent<JournalEvents>();
        DialogueScreen = GameObject.Find("DialogueScreen");
        moveScript = GameObject.Find("Player").GetComponent<PlayerMovementScript>();
        narrative = DialogueScreen.GetComponent<NarrativeDialogue>();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if(showEnvelope && isTimerTet == false)
        {
            timer = 3.0f;
            isTimerTet = true;
        }
        if (timer <= 0.0f && showEnvelope && isTimerTet)
        {
            SceneManager.LoadScene(0);
        }


        if (showEnvelope)
        {
            envelope.SetActive(true);

        }
    }

    private void OnCollisionEnter(Collision collision)
    {


        if (collision.gameObject.tag == "Player") //interaction with player 
        {
            if (journal.isPlayerActive && journal.isBugFoodActive && journal.isBugFood2Active && journal.isSnailActive && journal.isBeeActive)
            {
                //EndYes.json
                moveScript.currentMoveSpeedPlayer = 0;
                showYes = true;
                yesText.SetActive(true);


                /* if (showEnvelope)
                 {
                 envelope.SetActive(true);

                 }*/
                //timer = 8f;
                


            }
            else
            {
                noText.SetActive(true);
                //print("Nope");
                //Debug.Log(narrative.inkJSONAsset.GetType());
                //Story story = new Story();
                //maybe if able call narrative dialogue script and have the above then work?
                //thought process is that calling the story variable from the narrative dialogue script will allow for access to the .json files for making words
            }






        }
    }
}
