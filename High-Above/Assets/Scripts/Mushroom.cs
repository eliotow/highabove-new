﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{

    //private int health = 3;
    private float shrinktime = 1;
    public bool hasBee = false;

    public bool doShrink = false;
    public bool timerSet = false;
    public int color = 0; //0=Red 1=Green 2=Blue
    public Material red_Material;
    public Material green_Material;
    public Material blue_Material;

    // Use this for initialization
    void Awake()
    {

    }
    private void Start()
    {
        color = Random.Range(0, 3);        
    }
    // Update is called once per frame
    void Update()
    {
        if (color == 0)
            GetComponentInChildren<SkinnedMeshRenderer>().material = red_Material;
        if (color == 1)
            GetComponentInChildren<SkinnedMeshRenderer>().material = green_Material;
        if (color == 2)
            GetComponentInChildren<SkinnedMeshRenderer>().material = blue_Material;
        //if (health <= 0)
        //Destroy (gameObject);

        shrinktime -= Time.deltaTime;

        //mushroom shrink
        if (doShrink)
        {
            StartCoroutine(ShrinkSize(1));
            if (!timerSet)
            {
               shrinktime = 2;
               timerSet = true;
            }
        }

        if(shrinktime <= 0 && doShrink)
        {
            //print("turned off");
            doShrink = false;
            timerSet = false;
        }

        if (doShrink == false)
        {
            StopAllCoroutines();
            if(transform.localScale.y < .2f)
            {
                transform.localScale += new Vector3(.0002f, .0002f, .0002f);
            }
        }


    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //...
            //Debug.Log("player triggered mushroom");
            
        }
        
        if (other.tag == "bug")
        {
            //tickSource.Play();
            //GetComponent<ParticleSystem>().enableEmission = true; // turn on spore when bug reaches mushroom

			//StartCoroutine(ScaleOverTime(1)); //decrease scale continuously when bug reaches mushroom
            //Debug.Log("bug triggered mushroom");
           // StartCoroutine(ShrinkSize(1));

            print("test");
        }

		if (other.tag == "caterpillar") 
		{

		}

		//if (other.tag == "ants") 
		//{
			//health -= 1;
			//StartCoroutine(ShrinkSize(1)); 
		//}
        

    }


    IEnumerator ScaleOverTime(float time) //coroutine that causes object attached to this script to decrease in size over time and then destroys it
    {
        Vector3 originalScale = transform.localScale;
        Vector3 destinationScale = new Vector3(0f, 0f, 0f);

        float currentTime = -1.0f; //delay before starting to decrease size

        do
        {
            transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);

        Destroy(gameObject);

    }

	IEnumerator ShrinkSize(float time) //coroutine that causes object attached to this script to decrease in size over time and then destroys it
	{
		Vector3 originalScale = transform.localScale;
		Vector3 destinationScale = new Vector3(transform.localScale.x * .75f, transform.localScale.y *.75f, transform.localScale.z *.75f);

		float currentTime = -1.0f; //delay before starting to decrease size

		do
		{
			transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
			currentTime += Time.deltaTime;
			yield return null;
		} while (currentTime <= time);
	}

   

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "player")
        {
            //...
        }
        
    }

    public void playAnimation()
    {
        Animation shroomAnim;
        shroomAnim = gameObject.GetComponentInChildren<Animation>();
        shroomAnim.Play();
    }
}
