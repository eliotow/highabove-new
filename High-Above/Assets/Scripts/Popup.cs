﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup : MonoBehaviour
{
    public float destroyTime = 1.5f;
    public Vector3 offset = new Vector3(0, 0, 0);

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, destroyTime);

        transform.localPosition += offset;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.rotation = Quaternion.identity;
       // transform.LookAt(Camera.main.transform);
        transform.localEulerAngles = new Vector3(90, 0, 0);
        //transform.rotation.Set(transform.rotation.x, -transform.rotation.y, transform.rotation.z, transform.rotation.w);
    }
}
