﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class JournalEvents : MonoBehaviour
{
    public bool isPlayerActive = false;
    public bool isBugFoodActive = false;
    public bool isBugFood2Active = false;
    public bool isSnailActive = false;
    public bool isBeeActive = false;
    public bool IconCanAppear = true;
    GameObject journal;
    GameObject pauseMenu;

    public GameObject[] bugEntry;
    //public GameObject[] bugEntry2;
    public GameObject[] snailEntry;
    public GameObject[] hiddenBugEntry;
    public GameObject[] beeEntry;
    public GameObject[] bugFood1;
    public GameObject[] bugFood2;

    public GameObject alertIcon;
    public GameObject menuButton;

    public Image alertImage;
    //public SpriteRenderer spriteView;
    //public GameObject bug;
    bugFollow bugFollowScript;


    // Start is called before the first frame update
    void Start()
    {
        journal = GameObject.FindGameObjectWithTag("Journal");
        //bugFollowScript = bug.GetComponent<bugFollow>();
        bugEntry = GameObject.FindGameObjectsWithTag("BugEntry");
        //bugEntry2 = GameObject.FindGameObjectsWithTag("BugEntry2");
        snailEntry = GameObject.FindGameObjectsWithTag("SnailEntry");
        beeEntry = GameObject.FindGameObjectsWithTag("BeeEntry");
        bugFood1 = GameObject.FindGameObjectsWithTag("BeetleFood");
        bugFood2 = GameObject.FindGameObjectsWithTag("BeetleFood2");
        //alertIcon = GameObject.FindGameObjectWithTag("alertIcon");

        pauseMenu = GameObject.Find("PauseBG");
        
       //spriteView = alertIcon.GetComponent<SpriteRenderer>();
        

       //hiddenBugEntry = GameObject.FindGameObjectsWithTag("HiddenBugEntry");
        deactivate(bugEntry/*, hiddenBugEntry*/);
        deactivate(bugFood1/*, hiddenBugEntry*/);
        deactivate(bugFood2/*, hiddenBugEntry*/);
        deactivate(snailEntry/*, hiddenBugEntry*/);
        deactivate(beeEntry/*, hiddenBugEntry*/);
        journal.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(false);
     
        //  alertIcon.SetActive(false);
       // alertImage.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        

        if (isPlayerActive)
        {
            
            activate(bugEntry/*, hiddenBugEntry*/);
           
            
        }

        if (isBugFoodActive)
        {
            //print("WOOOO");
            activate(bugFood1/*, hiddenBugEntry*/);
            
            if (IconCanAppear == true)
           {
              
               // alertImage.enabled = true;
               // IconCanAppear = false;
               
            }
        }

        if (isBugFood2Active)
        {
            //print("WOOOO");
            activate(bugFood2/*, hiddenBugEntry*/);
            if (IconCanAppear == true)
            {

                // alertImage.enabled = true;
                // IconCanAppear = false;

            }
        }

        if (isSnailActive)
        {
            //print("WOOOO");
           if (IconCanAppear == true)
            {
                
                //IconCanAppear = false;
               // alertImage.enabled = true;
                
            }
            activate(snailEntry/*, hiddenBugEntry*/);
        }

        if (isBeeActive)
        {
            //Debug.Log("WOOOO");
            if (IconCanAppear == true)
            {

                //IconCanAppear = false;
                // alertImage.enabled = true;

            }
            activate(beeEntry/*, hiddenBugEntry*/);
        }
    }


    void activate(GameObject[] array/*, GameObject[] hidden*/)
    {
        for (int i = 0; i < array.Length; i++)
        {
                array[i].SetActive(true);
                //hidden[i].SetActive(false);
        }
    }

    void deactivate(GameObject[] array/*, GameObject[] hidden*/)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i].SetActive(false);
            //hidden[i].SetActive(true);
        }
    }

   public void disableIcon() {
        //if (alertImage.enabled == true) {
        //    alertImage.enabled = false;
          //  IconCanAppear = true;
      // }
        
        //Debug.Log("Spriteview is" + alertImage.enabled);
    }
    //The above function is called when the pause button is pressed and disables the icon
}
