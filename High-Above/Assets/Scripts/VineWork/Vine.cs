﻿

using SplineMesh;
using UnityEditor;
using UnityEngine;


/// <summary>
/// Growing Vine based on spline
/// </summary>
//[ExecuteInEditMode]
//[RequireComponent( typeof( SplineMeshTiling ) )]
//[RequireComponent( typeof( Spline ) )]
/*public class Vine : MonoBehaviour
{
    #region Fields
    /// <summary>
    /// transform of player. The growing vine will follow it
    /// </summary>
    [Tooltip( "transform of player. The growing vine will follow it" )]
    public Transform Player;
    /// <summary>
    /// position in world space of the beginning of the vine
    /// </summary>
    [Tooltip( "position in world space of the beginning of the vine" )]
    public Vector3 StartPosition = Vector3.zero;
    /// <summary>
    /// start diameter of the vine
    /// </summary>
    [Tooltip( "start diameter of the vine" )]
    public float StartScale = 1;
    /// <summary>
    /// distance from head the vine starts shrinking
    /// </summary>
    [Tooltip( "distance from head the vine starts shrinking" )]
    public float ThinLength = 5;
    /// <summary>
    /// growth distance that produce a new curve in the spline
    /// </summary>
    [Tooltip( "growth distance that produce a new curve in the spline" )]
    public float SpawDistance = 2;
    /// <summary>
    /// curve smoothness
    /// </summary>
    [Tooltip( "curve smoothness" )]
    public float CurveSmoothness = 0.8f;


    private Spline spline;
    //public float startScale = 1;

    //public float DurationInSecond;
    private SplineNode lastNode = null;
    private SplineNode currentNode = null;

    #endregion Fields

    #region Methods

    private void OnEnable()
    {
        Init();
#if UNITY_EDITOR
        EditorApplication.update += EditorUpdate;
#endif
    }
    void OnDisable()
    {
#if UNITY_EDITOR
        EditorApplication.update -= EditorUpdate;
#endif
    }

    private void OnApplicationQuit()
    {
        for ( int i = transform.childCount - 1; i >= 0; i-- )
        {
            GameObject.Destroy( transform.GetChild( i ).gameObject );
        }
    }

    private void OnValidate()
    {
        Init();
    }

    private void Update()
    {
        if ( spline == null )
            return;

        Vector3 currentNodeDirection = ( lastNode.Position - currentNode.Position ) * 0.1f;
        if ( ( Player.position - lastNode.Position ).sqrMagnitude > SpawDistance * SpawDistance )
        {
            currentNode = new SplineNode( Player.position, Vector3.zero );
            lastNode = spline.nodes[spline.nodes.Count - 1];
            spline.AddNode( currentNode );
            SplineMeshTiling smt = GetComponent<SplineMeshTiling>();
            if ( smt )
            {
               smt.CreateMeshes( ThinLength );
            }
        }

        currentNode.Position = Player.position;
        currentNode.Direction = currentNode.Position + currentNodeDirection;
        lastNode.Direction = Vector3.Lerp( lastNode.Position, currentNode.Position, CurveSmoothness );

        SetNodeScales();
    }


    void EditorUpdate()
    {
        //             return;
        //             rate += Time.deltaTime / DurationInSecond;
        //             if ( rate > 1 )
        //             {
        //                 rate--;
        //             }
        //             Contort();
    }

    private void SetNodeScales()
    {
        //             float nodeDistance = 0;
        //             float fullScaleNodeDistance = 0;
        //             for ( int i = 0; i < spline.nodes.Count; i++ )
        //             {
        //                 if ( i >= spline.nodes.Count - 1 - ThinNodes )
        //                 {
        //                     SplineNode n = spline.nodes[i];
        //                     float nodeDistanceRate = nodeDistance / ( spline.Length - fullScaleNodeDistance );
        //                     float nodeScale = Mathf.Max( 0.001f, StartScale * ( 1.0f - nodeDistanceRate ) );
        //                     n.Scale = new Vector2( nodeScale, nodeScale );
        //                     if ( i < spline.curves.Count )
        //                         nodeDistance += spline.curves[i].Length;
        //                 }
        //                 else
        //                     fullScaleNodeDistance += spline.curves[i].Length;
        //             }
        //             
        //             



        /*
                        float nodeDistance = 0;
                    for ( int i = 0; i < spline.nodes.Count; i++ )
                    {
                        SplineNode n = spline.nodes[i];
                        float nodeDistanceRate = nodeDistance / spline.Length;
                        n.Scale = Vector2.one * StartScale;

                        if ( spline.nodes.Count - i < ThinNodes )
                            n.Scale *= Mathf.Max( 0.001f, StartScale * ( 1.0f - nodeDistanceRate ) );

                        nodeDistance += spline.curves[i].Length;
                    }*/

        //             float nodeDistance = 0;
        //             for ( int i = 0; i < spline.nodes.Count - ThinNodes; i++ )
        //             {
        //                 SplineNode n = spline.nodes[i];
        //                 n.Scale = new Vector2( StartScale, StartScale );
        //                 if ( i < spline.curves.Count )
        //                     nodeDistance += spline.curves[i].Length;
        //             }
        // 
        //             float thinNodeDistance = 0;
        //             for ( int i = Mathf.Max( 0, spline.nodes.Count - ThinNodes ); i < spline.nodes.Count; i++ )
        //             {
        //                 SplineNode n = spline.nodes[i];
        //                 float nodeDistanceRate = thinNodeDistance / ( spline.Length - nodeDistance );
        //                 float nodeScale = Mathf.Max( 0.001f, StartScale * ( 1.0f - nodeDistanceRate ) );
        //                 n.Scale = new Vector2( nodeScale, nodeScale );
        //                 if ( i < spline.curves.Count )
        //                     thinNodeDistance += spline.curves[i].Length;
        //             }
        //             

       /* float nodeDistance = 0;
        int i = 0;
        float splineLength = spline.Length;
        foreach ( var n in spline.nodes )
        {
            float nodeDistanceRate = /*Mathf.Max( 0,*///( splineLength - nodeDistance ) / ThinLength /*)*/;/// spline.Length;
            //nodeDistanceRate *= nodeDistanceRate;
            //nodeDistanceRate = Mathf.Clamp( nodeDistanceRate, 0.01f, 1 ); //nodeDistance / spline.Length;
            //float nodeScale = StartScale * ( nodeDistanceRate );
            //n.Scale = new Vector2( nodeScale, nodeScale );
            //if ( i < spline.curves.Count )
            //{
              //  nodeDistance += spline.curves[i++].Length;
            //}
        //}
    //}

   /* private void Init()
    {
        transform.position = Vector3.zero;
        spline = GetComponent<Spline>();
        while ( spline.nodes.Count > 2 )
            spline.RemoveNode( spline.nodes[spline.nodes.Count - 1] );

        lastNode = spline.nodes[0];
        currentNode = spline.nodes[spline.nodes.Count - 1];
        lastNode.Position = StartPosition;
        lastNode.Direction = lastNode.Position;
        currentNode.Position = Player.position;
    }

    #endregion Methods
}*/
