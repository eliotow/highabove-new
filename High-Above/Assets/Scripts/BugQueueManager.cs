﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

[UnityEngine.Scripting.Preserve]
public class BugQueueManager : MonoBehaviour {
    public List<Transform> BugQueue = new List<Transform>();
    public float mindistance = 0.5f;
    public float moveSpeed = 1;
    

    private float dis;
    private Transform curBug;
    private Transform prevBug;
    public Transform cylinderTransform;
    public Vector3 cylinderAxis = new Vector3(0, -4, 0); // **update if base tree is ever moved; must be any point along y axis of cylinder

    // Use this for initialization
    void Start () {
        cylinderTransform = GameObject.FindWithTag("tree").transform;

    }
	
	// Update is called once per frame
	void Update () {
        
        BugLine();

        Debug.Log("BugQueManager Exists");
        //Debug.Log(BugQueue);

    }

    public void BugLine()// function to make bugs move after one another in a line
    {
        for (int i = 1; i < BugQueue.Count; i++)
        {
            curBug = BugQueue[i];
            Transform target = BugQueue[i - 1];

            if (mindistance < Vector3.Distance(curBug.position, target.position)) //don't move if you're already close enough
            {
                curBug.transform.position = Vector3.MoveTowards(curBug.transform.position, target.position, moveSpeed * Time.deltaTime);
                curBug.GetComponent<BugMovementScript>().stoppedAtPlayer = false; 
            }
            else
            {
                curBug.GetComponent<BugMovementScript>().stoppedAtPlayer = true;
            }

            if (0.5 < Vector3.Distance(curBug.position, target.position)) //don't look if you're on top of the player
            {
                Vector3 gravityUp = Vector3.ProjectOnPlane(curBug.transform.position - cylinderTransform.position, cylinderAxis);// these together work to make the bug look at the player when following
                curBug.transform.rotation = Quaternion.LookRotation(target.transform.position - curBug.transform.position, gravityUp);
            }
        }

    }

    public void RemoveFromBugQueue(Transform bug)
    {

        if (BugQueue.Contains(bug))
        {
            Debug.Log("Removing from queue: " + bug);
            BugQueue.Remove(bug);
        }
    }
}
