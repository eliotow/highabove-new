﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class OpenJournalTab : MonoBehaviour
{
    public enum JournalTab { PauseMenu, Snails, Beetles, BeetlesFood, BeetlesFood2, Summer, Bees }; //Rasmius };
    public JournalTab tab;

    public float maxSize = 100;
    public float minSize = 50;
    public float pulseSpeed = 1;
    public float normalSize = 70;

    public Button yourButton;
    public JournalEvents journalEvents;
    public InGameMenu inGameMenu;
    
    public GameObject pauseMenu;
    public GameObject JournalEntries;
    public GameObject snails;
    public GameObject beetles;
    public GameObject beetlesFood;
    public GameObject summer;
    public GameObject bees;
    //public GameObject rasmius;

    public bool seenSnails = false;
    public bool seenBeetles = false;
    public bool seenBeetlesFood = false;
    public bool seenBeetlesFood2 = false;
    public bool seenSummer = false;
    public bool seenBees = false;
    //public bool seenRasmius = false



    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

        tab = JournalTab.Summer;
        
    }

    private void Update()
    {
        if (tab == JournalTab.PauseMenu)
            GetComponent<RectTransform>().sizeDelta = new Vector2(normalSize, normalSize);
        else
        {
            float range = maxSize - minSize;
            float size = minSize + Mathf.PingPong(Time.time * pulseSpeed, range);
            GetComponent<RectTransform>().sizeDelta = new Vector2(size, size);
            
        }       
    }

    public void SetJournalTab(JournalTab t)
    {
        if (t == JournalTab.Beetles && !seenBeetles)
        {
            tab = t;
            seenBeetles = true;
            GetComponent<FMODUnity.StudioEventEmitter>().Play();
        }
        else if (t == JournalTab.BeetlesFood && !seenBeetlesFood)
        {
            tab = t;
            seenBeetlesFood = true;

            GetComponent<FMODUnity.StudioEventEmitter>().Play();
        }
        else if (t == JournalTab.BeetlesFood && !seenBeetlesFood2)
        {
            tab = t;
            seenBeetlesFood = true;

            GetComponent<FMODUnity.StudioEventEmitter>().Play();
        }
        else if (t == JournalTab.Snails && !seenSnails)
        {
            tab = t;
            seenSnails = true;
            GetComponent<FMODUnity.StudioEventEmitter>().Play();
        }
        else if (t == JournalTab.Summer && !seenSummer)
        {
            tab = t;
            seenSummer = true;
           // GetComponent<FMODUnity.StudioEventEmitter>().Play();
        }
        else if (t == JournalTab.Bees && !seenBees)
        {
            tab = t;
            seenBees = true;
            GetComponent<FMODUnity.StudioEventEmitter>().Play();
        }
        //else if (t == JournalTab.Rasmius && !seenRasmius)
        //tab = t;
    }

    void TaskOnClick()
    {
        if (tab == JournalTab.PauseMenu)
            pauseMenu.SetActive(true);
        else if (tab == JournalTab.Snails)
        {
            pauseMenu.SetActive(false);
            JournalEntries.SetActive(true);
            summer.SetActive(false);
            beetles.SetActive(false);
            bees.SetActive(false);
            snails.SetActive(true);
            beetlesFood.SetActive(false);

            //rasmius.SetActive(false);
        }
        else if (tab == JournalTab.Beetles) //|| tab == JournalTab.BeetlesFood)
        {
            pauseMenu.SetActive(false);
            JournalEntries.SetActive(true);
            summer.SetActive(false);
            snails.SetActive(false);
            bees.SetActive(false);
            beetles.SetActive(true);
            beetlesFood.SetActive(false);
            // rasmius.SetActive(false);
        }
        else if (tab == JournalTab.BeetlesFood || tab == JournalTab.BeetlesFood2)
        {
            pauseMenu.SetActive(false);
            JournalEntries.SetActive(true);
            summer.SetActive(false);
            snails.SetActive(false);
            bees.SetActive(false);
            beetles.SetActive(false);
            beetlesFood.SetActive(true);
            // rasmius.SetActive(false);
        }
        else if (tab == JournalTab.Summer)
        {
            pauseMenu.SetActive(false);
            JournalEntries.SetActive(true);
            beetles.SetActive(false);
            snails.SetActive(false);
            bees.SetActive(false);
            summer.SetActive(true);
            beetlesFood.SetActive(false);

        }
        else if (tab == JournalTab.Bees)
        {
            pauseMenu.SetActive(false);
            JournalEntries.SetActive(true);
            beetles.SetActive(false);
            snails.SetActive(false);
            summer.SetActive(false);
            bees.SetActive(true);
            beetlesFood.SetActive(false);

        }

        /* else if (tab == JournalTab.Rasmius)
         {
             pauseMenu.SetActive(false);
             JournalEntries.SetActive(true);
             beetles.SetActive(false);
             snails.SetActive(false);
             summer.SetActive(false);
             rasmius.SetActive(true);
         }*/

        journalEvents.disableIcon();
        inGameMenu.ToggleGame();

        tab = JournalTab.PauseMenu;
    }
}
