﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenu : MonoBehaviour
{
    public GameObject joystick;
    public GameObject[] journalEntry;
    public bool isOpen = false;

    public void PauseGame()
    {
        print("Pause Game ran");
        Time.timeScale = 0.0f;
        joystick.SetActive(false);
        //activate(journalEntry);
        isOpen = true;
    }
    public void ContinueGame()
    {
        print("Continue game ran");
        Time.timeScale = 1.0f;
        joystick.SetActive(true);
        deactivate(journalEntry);
        isOpen = false;
    }

    public void ToggleGame()
    {
        if(isOpen == false)
        {
            PauseGame();
        }
        else
        {
            ContinueGame();
        }
    }

    void activate(GameObject[] array/*, GameObject[] hidden*/)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i].SetActive(true);
            //hidden[i].SetActive(false);
        }
    }

    void deactivate(GameObject[] array/*, GameObject[] hidden*/)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i].SetActive(false);
            //hidden[i].SetActive(true);
        }
    }
}
