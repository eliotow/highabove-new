﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.UI;
using TMPro;

public class NarrativeDialogue : MonoBehaviour
{
    public TextAsset inkJSONAsset;
    private Story story;

    public Canvas canvas;
    public TextMeshProUGUI textBox;
    public Button button;

    EndLevel endlevel;
    public int express = 0;
    public GameObject normalFace;
    public GameObject starEyes;
    public GameObject questionFace;
    
    // Start is called before the first frame update
    void Start()
    {
        endlevel = GameObject.Find("SnailBoundaryTop").GetComponent<EndLevel>();

        if (textBox == null)
        {
            textBox = GameObject.Find("DialogueText").GetComponent<TextMeshProUGUI>();
            print(textBox);
        }

        //script for calling the .ink files located in the Narrative Dialogue script folder
        story = new Story(inkJSONAsset.text);
        //Debug.Log(story.Continue());

        //start the story off with the first line of text
        RefreshView();
    }

    //This is the main function called every time the story changes. It does a few things:
    //Destroys all the old content and choices.
    //Continues over all the lines of text, then displays all the choices. If there are no choices, the story is finished.
    public void RefreshView()
    {
        //Read all the content until we can't continue any more
        if (story.canContinue)
        {
            //Continue gets the next line of the story
            string text = story.Continue();
            //This removes any white space fromt he text
            text = text.Trim();
            //Display the text on the screen
            CreateContentView(text);
            
           
            if (express == 2)
            {
                normalFace.SetActive(false);
                questionFace.SetActive(true);
                starEyes.SetActive(false);
            }
            if (express == 4)
            {
                normalFace.SetActive(false);
                questionFace.SetActive(false);
                starEyes.SetActive(true);
            }
            express++;
        }
        //content has run out
        else
        {
            //GetComponent<DialogueMenuManager>
            CanvasGroup cg = GetComponent<CanvasGroup>();
            cg.alpha = 0;
            cg.interactable = false;
            cg.blocksRaycasts = false;
            if (endlevel.showYes)
            {
                endlevel.showEnvelope = true;

            }
        }
    }

    //Edits text on screen to match the text pulled from the Narrative Dialogue script
    void CreateContentView(string text)
    {
        textBox.text = text;
    }

}
