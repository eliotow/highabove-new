﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour {
    protected Joystick joystick;

    public float currentMoveSpeedPlayer;
    public Rigidbody body;

    public Vector3 moveDirection;
    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
        body = GetComponent<Rigidbody>();
        
    }
    void Update()
    {
        //moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized; //for in-engine testing
       moveDirection = new Vector3(joystick.Horizontal * currentMoveSpeedPlayer, 0, joystick.Vertical * currentMoveSpeedPlayer); //for mobile ports

        


        //Vector3 newPosition = new Vector3(joystick.Horizontal, joystick.Vertical, 0.0f); // this positioning makes it so it rotates
        //need to make the rhino beetle not turn model wholy but just along what is our vertical
        //transform.LookAt(newPosition + transform.position); // rotating the whole object to look

    }

    void FixedUpdate()
    {
        body.MovePosition(body.position + transform.TransformDirection(moveDirection) * currentMoveSpeedPlayer * Time.deltaTime);
      //  this.transform.rotation = Quaternion.LookRotation(moveDirection);
    }
}
