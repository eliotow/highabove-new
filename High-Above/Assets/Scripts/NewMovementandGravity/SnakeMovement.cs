﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

public class SnakeMovement : MonoBehaviour {

    public List<Transform> BodyParts = new List<Transform>();

    public float mindistance = 0 / 35f;

    public int beginsize;

    public float speed = 0;
    public float rotationspeed = 50;

    public GameObject bodyprefab;

    private float dis;
    private Transform curBodyPart;
    private Transform prevBodyPart;

    private bool headMoving = false;

    // Use this for initialization
    void Start()
    {

        for (int i = 0; i < beginsize - 1; i++)
        {
            AddBodyPart();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            Move();
        }
        

        if (Input.GetKey(KeyCode.Q))// for testing, adds more bodyparts to base of vine
            AddBodyPart();
	}

    public void Move()
    {
        float curspeed = speed;

        // if (Input.GetKey(KeyCode.W)) //move faster if w held down (works along automatic movement) **not joystick functional.**
        // curspeed *= 2;

        //BodyParts[0].Translate(BodyParts[0].forward * curspeed * Time.smoothDeltaTime, Space.World); // automatic movement of vine/snake

        if (Input.GetAxis("Horizontal") != 0)
        {
            BodyParts[0].Rotate(Vector3.up * rotationspeed * Time.deltaTime * Input.GetAxis("Horizontal"));
        }

        for (int i = 1; i < BodyParts.Count; i++)
        {
            curBodyPart = BodyParts[i];
            prevBodyPart = BodyParts[i - 1];

            dis = Vector3.Distance(prevBodyPart.position, curBodyPart.position);

            Vector3 newpos = prevBodyPart.position;

            //newpos.y = BodyParts[0].position.y;

            float T = Time.deltaTime * dis / mindistance * curspeed;

            if (T > 0.5f)
                T = 0.5f;
			
            curBodyPart.position = Vector3.Slerp(curBodyPart.position, newpos, T);
            curBodyPart.rotation = Quaternion.Slerp(curBodyPart.rotation, prevBodyPart.rotation, T);
			//prevBodyPart.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);


        }
    }

    public void AddBodyPart()
    {

        Transform newpart = (Instantiate(bodyprefab, BodyParts[BodyParts.Count - 1].position, BodyParts[BodyParts.Count - 1].rotation) as GameObject).transform;

        newpart.SetParent(transform);

        BodyParts.Add(newpart);

    }
}
