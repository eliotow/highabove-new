﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailGravityBody : MonoBehaviour {

    public TreeGravityScript attractorPlanet;
    private Transform characterTransform;
    //public Transform target;
    public Rigidbody rb;

    void Start()
    {
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

        rb = GetComponent<Rigidbody>();

        characterTransform = transform;
		attractorPlanet = GameObject.FindWithTag("tree").GetComponent<TreeGravityScript>();
    }

    void Update()
    {



    }

    void FixedUpdate()
    {
        if (attractorPlanet)
        {
            attractorPlanet.AttractSnail(characterTransform);


        }


    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("tree") && rb.isKinematic == false)
        {
            rb.isKinematic = true;
        }
    }
}
