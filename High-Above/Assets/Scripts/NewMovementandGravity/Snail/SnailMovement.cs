﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailMovement : MonoBehaviour {

    public float moveSpeed;

    public Transform cylinderTransform;

    public Transform target; //will move to this target if not null

    public Vector3 cylinderAxis = new Vector3(0, -4, 0); // **update if base tree is ever moved; must be any point along y axis of cylinder

    public float turnSpeed = 20; //For some reason this number is important, but changing it doesnt seem to do anything

    public float detectionDistance = 20; //how far raycast are sent out to detect objects for the snail

    JournalEvents journal;

    public bool catHasSap = false;

    //public bool isBeingFollowed;

    void Start()
    {
        journal = GameObject.Find("EventHandle").GetComponent<JournalEvents>();

        transform.rotation = Random.rotation; //may only work for spheres if no constraints

		cylinderTransform = GameObject.FindWithTag("tree").transform;


    }

    void Update()
    {
   
    }

    void FixedUpdate()
    {

        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);

        Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);

        //Follow Target
        if (target != null)
        {
            Quaternion rot = Quaternion.LookRotation(target.position - transform.position, Vector3.up);
            transform.eulerAngles = new Vector3(rot.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);

            float distance = Vector3.Distance(transform.position, target.position);
            if (distance < 1) //change this 1 to change how far they stop from the target
            {
                //I'm next to my target!
                Debug.Log("Snail reached target!");
                target = null; //set the new target here
                if(tag == "caterpillar")
                {
                    catHasSap = true;
                }
            }
        }
        else //target is null
        {
            //find a target. Use this if you always want the snail to have a target. 
            //Right now set to auto-find mushrooms
            GameObject[] gos;
            gos = GameObject.FindGameObjectsWithTag("sap"); //change this tag to sap
            target = gos[Random.Range(0, gos.Length - 1)].transform;
        }

        //Raycasting to make snail rotate away if it detects player/vine
        RaycastHit hit;
        var forward = transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(transform.position, forward, out hit, detectionDistance))
        {
            if ( hit.collider.tag == "bug" || hit.collider.tag == "bodypart" || hit.collider.tag == "snailBoundary" ||hit.collider.tag == "branch")
                {

                transform.Rotate(Vector3.up * 5 * Time.deltaTime * turnSpeed); // the "5" here is how much it turns each detection, like a re-direct angle

            }

            if (hit.collider.tag == "Player" )
            {

                transform.Rotate(Vector3.up * 5 * Time.deltaTime * turnSpeed); // the "5" here is how much it turns each detection, like a re-direct angle
                journal.isSnailActive = true;
                GameObject.Find("PauseButton").GetComponent<OpenJournalTab>().SetJournalTab(OpenJournalTab.JournalTab.Snails);
            }
            if (hit.collider.tag == "mushroom")
                {

                transform.Rotate(Vector3.up * 5 * Time.deltaTime * turnSpeed);

                }
            }      

    }

    private void OnTriggerEnter(Collider other)
    {

    }
}
