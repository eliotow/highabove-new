﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rhinoRotate : MonoBehaviour
{
    

    public PlayerMovementScript movement;

    public Vector3 rotDirect;


    //public Transform cylinderTransform;
    //public Vector3 cylinderAxis = new Vector3(0, -4, 0);

    private Animation anim;
    public GameObject animator;
    // Start is called before the first frame update
    void Start()
    {

        anim = animator.GetComponent<Animation>();
        

        // cylinderTransform = GameObject.FindWithTag("tree").transform;



        movement = GetComponentInParent<PlayerMovementScript>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
       // Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);
        // this.transform.rotation = Quaternion.LookRotation(joyHandle.transform.position -joyBase.transform.position,transform.up);


        //print(movement.moveDirection.x);



        //this.transform.rotation = Quaternion.LookRotation(movement.moveDirection, transform.up);
        if (movement.moveDirection.magnitude > Mathf.Epsilon)
        {
            float rX = movement.moveDirection.x;
            float rY = movement.moveDirection.y;
            float rZ = movement.moveDirection.z;



            rotDirect = rX * transform.parent.right + rZ * transform.parent.forward;
            /*
            rotDirect[0]=  rX;
            rotDirect[1] = rZ;
            rotDirect[2] = rY;
            */

            //print(rotDirect);

            this.transform.rotation = Quaternion.LookRotation(rotDirect, transform.parent.up);
            
            //print(movement.moveDirection);
            anim.Play();
        }
        else
        {
            anim.Stop();
        }


    }
}
