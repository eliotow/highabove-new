﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugGravityBody : MonoBehaviour {

    public TreeGravityScript attractorPlanet;
    private Transform characterTransform;
    //public Transform target;
    public Rigidbody rb;
    

    void Start()
    {
        attractorPlanet = GameObject.FindWithTag("tree").GetComponent<TreeGravityScript>();
        characterTransform = GameObject.FindWithTag("tree").transform;

        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        rb.constraints = RigidbodyConstraints.FreezeRotation;

        characterTransform = transform;
    }

    void Update()
    {

        if (GetComponent<BugMovementScript>().isFollowingPlayer == true)
        {
            // same thing can be called inside of BugMovementScript, not deleting cause scared to :))
        }

    }

    void FixedUpdate()
    {
        if (attractorPlanet)
        {
            attractorPlanet.AttractBug(characterTransform);


        }
        

    }

    private void OnCollisionEnter(Collision collision)
    {
        /*if (collision.gameObject.CompareTag("tree") && rb.isKinematic == false)
        {
            rb.isKinematic = true;
        }*/
    }
}
