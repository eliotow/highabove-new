﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[UnityEngine.Scripting.Preserve]
public class BugMovementScript : MonoBehaviour
{
    public GameObject playerPopupPrefab;
    public GameObject shroomPopupPrefab;
    public GameObject snailPopupPrefab;
    public GameObject shroomConfusion;

    public bool grown = false;

    public bool IconCanAppear = true;
   // public GameObject alertIcon;
    public GameObject menuButton;
   // public Image alertImage;

    public bool amRed;
    public bool amBlue;
    public bool amGreen;

    // for Bug Lineup
    /*public  List<Transform> BugQueue = new List<Transform>();
    public float mindistance = 5;

    private float dis;
    private Transform curBug;
    private Transform prevBug;
   */
    BugQueueManager BugQueueManager;
    PlayerMovementScript PlayerMovementScript;

    //Move Speeds
    public float moveSpeed = 1.2f;

    private float startingMoveSpeed = 1.2f;

    public float rotationspeed = 50;

    //public float boostSpeed = 0.3f;

    //Timer for speed boost
   // private float boostTimer = 1.5f;

    //References
    public Transform target;

    public Vector3 target2;

    GameObject Player;

    JournalEvents journal;

    Mushroom mushScript;

    //Other Value Trackers
    public float growthCounter = 1;


    //Booleans
    public bool isFollowingPlayer;
    [HideInInspector]
    public bool stoppedAtPlayer;

    private bool moveable;

    private bool foundMushroom;

    public bool isFollowingSnail;

    public bool neverDoneSlow; //weird name, but used so bugs only slow the vine once while following it

    //public bool neverDoneReset;

    //public bool neverDoneDecay;

    //private bool startBoostTimer;

    private bool foundSap;

    public bool isCarrying;



    //Tree Gravity info
    public Transform cylinderTransform;

    public Vector3 cylinderAxis = new Vector3(0, -4, 0); // **update if base tree is ever moved; must be any point along y axis of cylinder



    void Start()
    {
        

        journal = GameObject.Find("EventHandle").GetComponent<JournalEvents>();
        mushScript = GameObject.Find("RedShroom").GetComponent<Mushroom>();

       //alertIcon.SetActive(false);
      //  alertImage.enabled = false;


        target = GameObject.FindWithTag("Player").transform;
        cylinderTransform = GameObject.FindWithTag("tree").transform;

        Player = GameObject.FindWithTag("Player");
        PlayerMovementScript = Player.GetComponent<PlayerMovementScript>();

        BugQueueManager = Player.GetComponent<BugQueueManager>();
        
        


        moveable = true;
        isFollowingPlayer = false;
        stoppedAtPlayer = false;
        isFollowingSnail = false;
        foundMushroom = false;
        neverDoneSlow = true;
        //neverDoneReset = false;
        //neverDoneDecay = true;
        //startBoostTimer = false;
        foundSap = false;
        isCarrying = false;



    }

    void Update()
    {
        /*if (neverDoneSlow)
        {
            if (isFollowingPlayer == true)
            {
                PlayerMovementScript.currentMoveSpeedPlayer = PlayerMovementScript.currentMoveSpeedPlayer - 0.1f;
                neverDoneSlow = false;//this enables the bug to slow the player movement, but only once while following (this will be reset when bug follows snail, so that when it follows back to player the slow still applies)
                neverDoneReset = true;
            }

        }*/
        if (target == null)
            target = GameObject.FindWithTag("Player").transform;
        if (Player == null)
            Player = GameObject.FindWithTag("Player");
        if (BugQueueManager == null)
        {
            if (Player.GetComponent<BugQueueManager>() != null)
                BugQueueManager = Player.GetComponent<BugQueueManager>();
            else
                BugQueueManager = Player.GetComponentInParent<BugQueueManager>();
        }
        //Debug.Log(BugQueueManager);

        if ((isFollowingPlayer == true && stoppedAtPlayer == false) || isFollowingSnail == true)
        {
            GetComponentInChildren<Animation>().enabled = true;
        }

        if((isFollowingPlayer == false || stoppedAtPlayer == true) && isFollowingSnail == false)
        {
            
            GetComponentInChildren<Animation>().enabled = false;
        }

        /*if (neverDoneReset)
        {
            if(isFollowingPlayer == false)
            {
                PlayerMovementScript.currentMoveSpeedPlayer = PlayerMovementScript.currentMoveSpeedPlayer + 0.1f;
                StartCoroutine(speedBoost());
                
                neverDoneReset = false; // this does the opposite of the above; allows bugs to give back movement speed once they are sent off the vine
            }
        }

        if (startBoostTimer)
        {
            boostTimer -= Time.deltaTime;
        }

        if (neverDoneDecay)
        {
            if (boostTimer <= 0)
            {
                PlayerMovementScript.currentMoveSpeedPlayer = PlayerMovementScript.currentMoveSpeedPlayer - boostSpeed;
                neverDoneDecay = false;
            }
        }*/

    }

    void FixedUpdate()
    {
        
        
        //GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.TransformDirection(moveDirection) * moveSpeed * Time.deltaTime);
        if (isFollowingPlayer == true )
        {
           
            gameObject.layer = 31;

            //transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);

            isFollowingSnail = false; //turns off speed modification

            //moveSpeed = startingMoveSpeed;

            /*
            transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
            moveSpeed = 0.5f;

            Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);// these together work to make the bug look at the player when following
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position, gravityUp);
            */




        }
        
        if(isFollowingPlayer == false)
        {
            gameObject.layer = 16;
            //Debug.Log(BugQueueManager.name + ", "  + ", " + transform);
            //Debug.Log(BugQueueManager.BugQueue.Count);
            if (BugQueueManager != null)
                BugQueueManager.RemoveFromBugQueue(this.transform);
            //if (BugQueueManager.BugQueue.Contains(transform))
                //BugQueueManager.BugQueue.Remove(transform);
        }


        /*if(isFollowingPlayer == true & BugQueueManager.BugQueue.Contains(transform))
        {
            BugLine();
            
        }*/
    
        if (foundMushroom == true)
        {
          transform.position = Vector3.MoveTowards(transform.position, target2, moveSpeed * Time.deltaTime); //once the bug finds a mushroom, it will go to it
            StartCoroutine(BugShroomReset());//calls a routine that makes foundMushroom false after 1.5 seconds; thought this was a fix but it wasn't, might be vestigial now
            
             

        }

        if (isFollowingSnail == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
            moveSpeed = 0.5f;

            Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position, gravityUp);
            //neverDoneSlow = true; //resets this boolean so the bug can slow the vine down again if it reaattaches to the vine from a snail
        }

        if (foundSap == true && grown == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);

            Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position, gravityUp);
            //neverDoneSlow = true;

            StartCoroutine (GrowOverTime(1));

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && moveable == true) // when bug zone finds player, make isFollowing true (follow the player)
        {
            if (BugQueueManager != null)
            {
                if (!BugQueueManager.BugQueue.Contains(other.transform))
                {
                    if (!BugQueueManager.BugQueue.Contains(other.transform))
                        BugQueueManager.BugQueue.Add(other.transform);
                }
            }
            target = other.gameObject.transform/*.GetChild(0).transform*/;

            if(isFollowingPlayer == false)
            {
                isFollowingPlayer = true;
                playerPopup();

                if (IconCanAppear == true)
                {
                   
                  //  alertImage.enabled = true;
                   // if (alertImage.enabled)
                    //{
                        //print("ahhh");
                    //}
                    //IconCanAppear = false;

                }

            }
            if (BugQueueManager != null)
                if (!BugQueueManager.BugQueue.Contains(transform))
                    BugQueueManager.BugQueue.Add(transform);

            journal.isPlayerActive = true;
            GameObject.Find("PauseButton").GetComponent<OpenJournalTab>().SetJournalTab(OpenJournalTab.JournalTab.Beetles);

        }

        if (other.tag == "mushroom" && isFollowingPlayer == true)
        {

            if (foundMushroom == false)
            {
                //red on red
                if (other.GetComponentInParent<Mushroom>().color == 0 && amRed  ) {
                    //print("red");
                    foundMushroom = true;
                    shroomPopup();
                    
                    //other.GetComponent<Mushroom>().doShrink = true;
                    other.GetComponentInParent<Mushroom>().doShrink = true;
                   
                }

                //green on green
                else if (other.GetComponentInParent<Mushroom>().color == 1 && amGreen)
                {
                    //print("green");
                    foundMushroom = true;
                    shroomPopup();

                    //other.GetComponent<Mushroom>().doShrink = true;
                    other.GetComponentInParent<Mushroom>().doShrink = true;


                    journal.isBugFoodActive = true;
                    GameObject.Find("PauseButton").GetComponent<OpenJournalTab>().SetJournalTab(OpenJournalTab.JournalTab.BeetlesFood);
                }

                //blue on blue
                else if (other.GetComponentInParent<Mushroom>().color == 2 && amBlue)
                {
                    //print("blue");
                    foundMushroom = true;
                    shroomPopup();

                    //other.GetComponent<Mushroom>().doShrink = true;
                    other.GetComponentInParent<Mushroom>().doShrink = true;


                    journal.isBugFoodActive = true;
                    GameObject.Find("PauseButton").GetComponent<OpenJournalTab>().SetJournalTab(OpenJournalTab.JournalTab.BeetlesFood);
                }
                else {
                    foundMushroom = true;
                    shroomConfusedPopup();
                    journal.isBugFood2Active = true;
                    GameObject.Find("PauseButton").GetComponent<OpenJournalTab>().SetJournalTab(OpenJournalTab.JournalTab.BeetlesFood);

                }



                Animation shroomAnim;
                shroomAnim = other.gameObject.GetComponentInChildren<Animation>();
                shroomAnim.Play();
                
                if (IconCanAppear == true)
                {
                    //print("ahhh");
                   // alertImage.enabled = true;

                    IconCanAppear = false;

                }
            }
            
            

           isFollowingPlayer = false; //when bug zone finds mushroom, stop moving the bug
            stoppedAtPlayer = false;    
           //moveable = false; //makes bug unable to be picked up again by player

           target2 = other.gameObject.transform.position; //gets the position of the found mushroom

           // journal.isBugFoodActive = true;
           // GameObject.Find("PauseButton").GetComponent<OpenJournalTab>().SetJournalTab(OpenJournalTab.JournalTab.BeetlesFood);
        }

        if (other.tag == "snail" && moveable == true)
        {
            isFollowingPlayer = false; // allows speed to be modified
            stoppedAtPlayer = false;
            target = other.gameObject.transform;


            if (isFollowingSnail == false)
            {
                isFollowingSnail = true;
                snailPopup();
                if (IconCanAppear == true)
                {
                   // //print("ahhh");
                   // alertImage.enabled = true;

                    IconCanAppear = false;

                }
            }
            
            

        }

        if (other.tag != "sap")
        {
            foundSap = false;
        }

        if (other.tag == "sap" && moveable == true)
        {
            
            isFollowingPlayer = false;
            stoppedAtPlayer = false;
            target = other.gameObject.transform;
            foundSap = true;
        }




    }

    IEnumerator GrowOverTime(float time)
    {
        Vector3 originalScale = transform.localScale;
        Vector3 destinationScale = new Vector3(transform.localScale.x * 1.25f, transform.localScale.y * 1.25f, transform.localScale.z * 1.25f);

        float currentTime = -1.0f;

        do
        {
            transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);

        foundSap = false;
        grown = true;
    }

    private void OnCollisionEnter(Collision collision)
    {


        if (collision.gameObject.tag == "Player") //interaction with player (add conditions for bug size/color/speed)
        {
            //isFollowing = false;
            //moveable = false;
            


        }


    }

    IEnumerator BugShroomReset()
    {
        yield return new WaitForSeconds(1.5f);
        foundMushroom = false;
    }

   /* IEnumerator speedBoost()
    {
        PlayerMovementScript.currentMoveSpeedPlayer = PlayerMovementScript.currentMoveSpeedPlayer + boostSpeed;
        startBoostTimer = true;
        yield return new WaitForSeconds(0.1f);

    }*/

    public void BugLine()// function to make bugs move after one another in a line
    {
        /*if (Input.GetAxis("Horizontal") != 0)
        {
            BugQueue[0].Rotate(Vector3.up * rotationspeed * Time.deltaTime * Input.GetAxis("Horizontal"));
        }

        for (int i = 1; i < BugQueue.Count; i++)
        {
            curBug = BugQueue[i];
            prevBug = BugQueue[i - 1];

            dis = Vector3.Distance(prevBug.position, curBug.position);

            Vector3 newpos = prevBug.position;

            //newpos.y = BodyParts[0].position.y;

            float T = Time.deltaTime * dis / mindistance * moveSpeed;

            if (T > 0.5f)
                T = 0.5f;
            curBug.position = Vector3.Slerp(curBug.position, newpos, T);
            curBug.rotation = Quaternion.Slerp(curBug.rotation, prevBug.rotation, T);


        }
        */

    }

    public void disableIcon()
    {
        //if (alertImage.enabled == true)
       // {
        //    alertImage.enabled = false;
        //    IconCanAppear = true;
        //}
    }


        void playerPopup()
    {
        Instantiate(playerPopupPrefab, transform.position, Quaternion.identity  ,transform);
        
    }

    void shroomPopup()
    {
        Instantiate(shroomPopupPrefab, transform.position, Quaternion.identity, transform);

    }

    void snailPopup()
    {
        Instantiate(snailPopupPrefab, transform.position, Quaternion.identity, transform);

    }

    void shroomConfusedPopup()
    {
        Instantiate(shroomConfusion, transform.position, Quaternion.identity, transform);
    }

}
