﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMovementSimple : MonoBehaviour
{
    public GameObject target;
    private GameObject prevTarget;
    private Vector3 arcPoint = new Vector3();
    private float flyTime = 0.0f;
    public float speed = 1.0f;
    public float arcDistance = 5.0f;
    
    // determine an initial value by checking where speedFactor converges
    float speedFactor = 0f;
    float targetStepSize = 0f; // divide by fixedUpdate frame rate
    float lastStepSize = 0;

    public float waitTimeMin = 10f;
    public float waitTimeMax = 30f;
    float waitTimeNext = 30f;
    float waitTime = 0f;

    bool landed = false;
    public bool triggerMovement = false;

    //mushroom colors
    public int color = 0;
    public Material red_Material;
    public Material green_Material;
    public Material blue_Material;

    JournalEvents journal;

    // Start is called before the first frame update
    void Start()
    {
        journal = GameObject.Find("EventHandle").GetComponent<JournalEvents>();


        if (target != null) //should start null, but fint arcPoint if first target is specified
            arcPoint = transform.position + (target.transform.position - transform.position) / 2 + Vector3.forward * arcDistance; // Play with 5.0 to change the curve

        speedFactor = speed / 10;
        targetStepSize = speed / 60f; // divide by fixedUpdate frame rate

        waitTimeNext = Random.Range(waitTimeMin, waitTimeMax);

        color = Random.Range(0, 3);
        if (color == 0)
            GetComponentInChildren<SkinnedMeshRenderer>().material = red_Material;
        if (color == 1)
            GetComponentInChildren<SkinnedMeshRenderer>().material = green_Material;
        if (color == 2)
            GetComponentInChildren<SkinnedMeshRenderer>().material = blue_Material;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (target == null || triggerMovement)
        {
            triggerMovement = false;
            landed = false;
            if (target != null)
                target.GetComponentInParent<Mushroom>().hasBee = false;
            FindClosestLanding();
        }
        else
        {
            target.GetComponentInParent<Mushroom>().hasBee = true;            
            if (flyTime < 1.0f)
            {
                landed = false;

                // Take a note of your previous position.
                Vector3 previousPosition = transform.position;

                //Bezier Curve
                Vector3 m1 = Vector3.Lerp(transform.position, arcPoint, flyTime);
                Vector3 m2 = Vector3.Lerp(arcPoint, target.transform.position, flyTime);
                gameObject.transform.position = Vector3.Lerp(m1, m2, flyTime);

                // Measure your movement length
                lastStepSize = Vector3.Magnitude(transform.position - previousPosition);

                // Accelerate or decelerate according to your latest step size.
                if (lastStepSize < targetStepSize)
                {
                    speedFactor *= 1.1f;
                }
                else
                {
                    speedFactor *= 0.9f;
                }

                flyTime += speedFactor * Time.deltaTime;

                transform.rotation = Quaternion.LookRotation(m2 - transform.position, Vector3.up);
            }
            else // landed
            {

                if (!landed)
                {
                    target.GetComponentInParent<Mushroom>().color = color;
                    target.GetComponentInParent<Mushroom>().playAnimation();
                }
                landed = true;

                waitTime += Time.deltaTime;
                if (waitTime >= waitTimeNext)
                {
                    waitTime = 0;
                    waitTimeNext = Random.Range(waitTimeMin, waitTimeMax);
                    triggerMovement = true;
                }
            }
        }
    }

    public void FindClosestLanding()
    {        
        flyTime = 0.0f;        
        
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("beeStopper");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                if (!go.Equals(target) && !go.Equals(prevTarget) && !go.GetComponentInParent<Mushroom>().hasBee)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
        }
        if (closest == null)
            closest = prevTarget;

        prevTarget = target;
        target = closest;

        Vector3 dir = Vector3.forward;
        if (Mathf.Abs(target.transform.position.x - transform.position.x) > Mathf.Abs(target.transform.position.z - transform.position.z))
        {
            //x movement
            if (target.transform.position.z > 0)
                dir = Vector3.forward;
            else
                dir = Vector3.back;
        }
        else
        {
            //z movement
            if (target.transform.position.x > 0)
                dir = Vector3.right;
            else
                dir = Vector3.left;
        }
        

        arcPoint = transform.position + (target.transform.position - transform.position) / 2 + dir * arcDistance; // Play with 5.0 to change the curve
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (landed)
            {
                Debug.Log("Landed");
                triggerMovement = true;
                waitTime = 0;
                journal.isBeeActive = true;
                GameObject.Find("PauseButton").GetComponent<OpenJournalTab>().SetJournalTab(OpenJournalTab.JournalTab.Bees);
            }
        }
    }
}
