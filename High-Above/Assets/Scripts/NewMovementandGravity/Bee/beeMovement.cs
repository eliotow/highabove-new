﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class beeMovement : MonoBehaviour {

    private Vector3 pivot = new Vector3(0, 0, 0);
    public float moveSpeed;
    public float rotSpeed;

    public Transform target;

    public Transform player;

    public Vector3 target2;

    public bool isFollowing;

    private bool moveable;

    private bool foundMushroom;

    private Vector3 offset;

    private Vector3 resetPosition;

    public float offsetNum;

    public Transform cylinderTransform;

    public Vector3 cylinderAxis = new Vector3(0, -4, 0); // **update if base tree is ever moved; must be any point along y axis of cylinder

    private bool beeResetHappened;

    private float timer;
    public float timerSet = 3;
    public bool beeswapped = false;

    public bool onSnail = false;

    public bool wantToPushBee = false;

    void Start()
    {
        cylinderTransform = GameObject.FindWithTag("tree").transform;
        moveable = true;
        isFollowing = false;
        foundMushroom = false;
        //offset = new Vector3(target.position.x+ offsetNum, target.position.y + offsetNum, target.position.z + offsetNum);

        beeResetHappened = true;
        timer = timerSet;
    }

    void Update()
    {
        //offset = new Vector3(target.position.x + offsetNum, target.position.y + offsetNum, target.position.z + offsetNum);
        
        //timer for follow cycle
        if (timer > 0)
        {
            timer -= 0.5f * Time.deltaTime;
            //print(timer);
            //print(isFollowing);
        }
        if ( timer <= 0 & isFollowing == false)
           
        {
           isFollowing = true;
           timer = timerSet;                  
        }

        if (onSnail)
        {
			//target = target.gameObject.transform.Find ("AttachPoint");
			transform.position = target.gameObject.transform.Find ("AttachPoint").position;
        }

        transform.LookAt(target.transform);

        
    }

    void FixedUpdate()
    {

        if (isFollowing == false)
        {
            
            transform.RotateAround(target.position, target.up, rotSpeed * Time.deltaTime);
        }

        if (isFollowing == false && beeResetHappened == false)
        {
           
            //StartCoroutine(ResetBee());
        }
        //GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.TransformDirection(moveDirection) * moveSpeed * Time.deltaTime);
        if (isFollowing == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
                    
            Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);// these together work to make the bug look at the player when following,
            transform.rotation = Quaternion.LookRotation(target.position - transform.position, gravityUp);// but require tree reference for each bug!! (find a fix?)

            beeResetHappened = false; //works with the ResetBee coroutine so it doesn't keep repeating

            

        }

        if (foundMushroom == true)
        {
          transform.position = Vector3.MoveTowards(transform.position, target2, moveSpeed * Time.deltaTime); //once the bug finds a mushroom, it will go to it
            //StartCoroutine(BugShroomReset());//calls a routine that makes foundMushroom false after 1.5 seconds; thought this was a fix but it wasn't, might be vestigial now
            

        }
    }

    private void OnTriggerEnter(Collider other)
    {
		if(other.transform != target && !other.gameObject.CompareTag("tree") && !other.CompareTag("branch") && !other.CompareTag("bodypart") && !other.CompareTag("snailBoundary") && !other.CompareTag("snail") && !other.CompareTag("beePusher") && !other.CompareTag("bee"))
			//& !other.CompareTag("bug"))
			{
			//if (other.tag == "snail")
				//Debug.Log ("Found snail");
			
			target = other.transform;
            StartCoroutine(DisableDetection());
           
            
				//print(other);
			}

        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(ResetBee());
            StartCoroutine(MoveTowardsPlayer());
        }

            // & other.CompareTag("bee")
                //& other.CompareTag("Untagged") & other.CompareTag("bodypart"))

		/*if (other.gameObject.CompareTag ("Player")) {
			target = other.transform;
			isFollowing = true;
		}

		if (!other.gameObject.CompareTag ("Player")) {
			target = other.transform;
			isFollowing = false;
		}*/



        
        

    }

    private void OnCollisionEnter(Collision collision)
    {


        
        //timer = timerSet;
        if (collision.gameObject.tag == "snail") //interaction with snail
        {
            isFollowing = true;
            onSnail = true;


        }

		if (collision.gameObject.tag == "ants") //makes the ants fall off the tree if the bee gets to them
		{
            StartCoroutine(ResetBee());
            collision.gameObject.GetComponent<AntsGravityBody> ().enabled = false;
			collision.gameObject.GetComponent<AntsMovement> ().enabled = false;
			collision.rigidbody.useGravity = true;
			collision.rigidbody.detectCollisions = false;

		}

        if (collision.gameObject.tag == "caterpillar")
        {
            /*StartCoroutine(DisableDetection());
            collision.gameObject.GetComponent<CaterpillarGravityBody>().enabled = false;
            collision.gameObject.GetComponent<CaterpillarMovement>().foundBee = true;
            
            collision.gameObject.GetComponent<CaterpillarMovement>().target = transform;
            StartCoroutine(ResetBee());*/


        }

        if (collision.gameObject.tag == "mushroom")
        {
            StartCoroutine(ResetBee());
        }


    }

    

    IEnumerator BugShroomReset()
    {
        yield return new WaitForSeconds(1.5f);
        foundMushroom = false;
    }

    IEnumerator ResetBee()
    {
        GetComponent<SphereCollider>().enabled = false;

        yield return new WaitForSeconds(.5f);
        wantToPushBee = true;
		yield return new WaitForSeconds(1f);
        wantToPushBee = false;
        GetComponent<Rigidbody>().velocity = Vector3.zero;

        isFollowing = false;
        transform.RotateAround(target.position, target.up, rotSpeed * Time.deltaTime);

        yield return new WaitForSeconds(3f);
        GetComponent<SphereCollider>().enabled = true;

        beeResetHappened = true;
        
 
    }

    IEnumerator DisableDetection()
    {
        GetComponent<SphereCollider>().enabled = false;

        yield return new WaitForSeconds(4.5f);

        GetComponent<SphereCollider>().enabled = true;
    }

    IEnumerator MoveTowardsPlayer()
    {
        isFollowing = true;
        yield return new WaitForSeconds(0.5f);
        isFollowing = false;
    }

}
